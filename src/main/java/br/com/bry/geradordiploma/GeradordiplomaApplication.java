package br.com.bry.geradordiploma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeradordiplomaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeradordiplomaApplication.class, args);
	}

}
