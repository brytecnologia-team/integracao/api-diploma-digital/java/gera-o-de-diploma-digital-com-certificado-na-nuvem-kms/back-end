package br.com.bry.geradordiploma.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import br.com.bry.geradordiploma.service.GeradorDiplomaService;

@CrossOrigin(origins ="http://localhost:3000")
@Controller
@RestController
@RequestMapping
public class GeradorDiplomaController {
	
	@Autowired
	private GeradorDiplomaService service;
	
	@PostMapping(value = "/XMLDiplomado/assinaKms")
	public ResponseEntity<String> assinaXMLPublico(
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		return ResponseEntity.status(HttpStatus.OK).body(service.assinaXMLDiplomado(request));
	}
	
	@PostMapping(value = "/XMLDocumentacaoAcademica/assinaKms")
	public ResponseEntity<String> assinaXMLPrivado(
			HttpServletRequest request, HttpServletResponse response) throws IOException{
		return ResponseEntity.status(HttpStatus.OK).body(service.assinaXMLDocumentacao(request));
	}
	
	@PostMapping(value = "/XMLDiplomado/copia-nodo")
	public ResponseEntity<String> copiaNodo(
			@RequestPart(value = "documentacaoAcademica", required = true) MultipartFile documentacaoAcademica,
			@RequestPart(value = "XMLDiplomado", required = true) MultipartFile xMLDiplomado)
					throws IOException, ParserConfigurationException, SAXException,
					TransformerFactoryConfigurationError, TransformerException {
		return ResponseEntity.status(HttpStatus.OK).body(service.copiaNodo(documentacaoAcademica, xMLDiplomado));
	}
}
