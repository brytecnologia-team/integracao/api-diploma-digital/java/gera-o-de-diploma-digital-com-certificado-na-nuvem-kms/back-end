package br.com.bry.geradordiploma.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeradorDiplomaRequest {
	private String tipoAssinante;
	@JsonProperty("CPFAssinante")
	private String CPFAssinante;

	public String getTipoAssinante() {
		return tipoAssinante;
	}

	public void setTipoAssinante(String tipoAssinante) {
		this.tipoAssinante = tipoAssinante;
	}

	public String getCPFAssinante() {
		return CPFAssinante;
	}

	public void setCPFAssinante(String CPFAssinante) {
		this.CPFAssinante = CPFAssinante;
	}





}
