package br.com.bry.geradordiploma.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.server.ResponseStatusException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service
public class GeradorDiplomaService {
	
	// TOKEN DE ACESSO AO BRY FRAMEWORK
    //Você pode emitir um token valido em: https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
	String authorization = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIcVFrTkVFVzJJdzNuU0JPcklDUU9KQmVWQUNhVjVuYzBuZG9TSWhfY2NBIn0.eyJleHAiOjE2NzY0NzY1ODAsImlhdCI6MTY3NjQ3NDc4MCwianRpIjoiY2ZkNDk1YTYtY2UyMC00MmZmLWJhZDgtZjYzZjk1ODU1MjhkIiwiaXNzIjoiaHR0cHM6Ly9jbG91ZC1ob20uYnJ5LmNvbS5ici9hdXRoL3JlYWxtcy9jbG91ZCIsImF1ZCI6WyJrbXMiLCJhY2NvdW50Il0sInN1YiI6ImY6ZWExZDg2NGYtNzg3MS00M2Q2LWJjYmYtMTE4N2M3ZmI4MTg2OmZpbGlwZS5kYXZpbGEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ0ZXJjZWlyb3MiLCJzZXNzaW9uX3N0YXRlIjoiMzY1NzliZWQtZjczMy00NjIyLThjNmEtMzg5ZWVlMzdhMDEyIiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJrbXMuc3Vwb3J0ZSIsImttcy5hZG1pbmlzdHJhZG9yIiwib2ZmbGluZV9hY2Nlc3MiLCJncCIsImFkbWluIiwiZ2EiLCJ1c3VhcmlvIiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiY29kZSI6IjA4OTc2MzY2OTQ4IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJjbG91ZF9hcHBfa2V5IjoiYXBwLmZpbGlwZS5kYXZpbGEuMTYxOTgwNzE5NTk1NSIsIm5hbWUiOiJGaWxpcGUgU2VyZW5hIEQnYXZpbGEiLCJjbG91ZF90b2tlbiI6Iko3VTVxMGNDUFdBMXdMeVl3cGJYNFNyNVgxTzRMS0VWSFI1aGo0MW5SQ2M1MGFUYVNyWmFuN0dUbnBRNHV6YmIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJmaWxpcGUuZGF2aWxhIiwiZ2l2ZW5fbmFtZSI6IkZpbGlwZSIsImZhbWlseV9uYW1lIjoiU2VyZW5hIEQnYXZpbGEifQ.fwfEIJl0A2EWaMTGhB-ZcYzQZUp1FbP3GkMWyWJATsGU96z-fzy0PGvd8Y9ktyueP1huIC7GsGJLu_0So_ZdAtZdt73P1YttUMDKAtgpE5w3Q4XvKChnghh_e2VkoU0Y9oZVUrYoBq_KRs9ydl_eJ_Q1Q-TxU0yGnfyIRijlqYyiQ6xtXHmatZF382rF23F9HngG8R1QfqEUhaLW-RfpZuYXlxfdDA_9v7x1hnTIyDSRItNA1yp_NhtYy9Da3uCjRE5hW6ifTZC7DIu2_L9l3-yKK6068viVzXCkkqQO15pYnO67ZO-KGEkNgADQU0KRKD5HkguZG9uB9mmiDer8CA";
	
	//URL PARA A QUAL SERÁ ENVIADA A REQUISIÇÃO POST. (Homologação ou Produção)
	//String url = "https://hub2.bry.com.br/api/xml-signature-service/v1/signatures/kms";
	String url = "https://hub2.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms";
	
	// TIPO DE RETORNO, SE RETURNTYPE FOI CONFIGURADO PARA BASE64, É RETORNADO UM
	// ARRAY COM AS ASSINATURAS CODIFICADAS EM BASAE64.
	// SE RETURNTYPE FOI DEFINIDO COMO LINK, É RETORNADO O SEGUINTE JSON:
	// identificador: Identificador do lote.
	// quantidadeAssinaturas: Quantidade total de assinaturas realizadas no lote.
	// documentos: Array de documentos assinados.
	// hash: Hash do documento. É o seu identificador único.
	// links: Array com link, seguindo o princípio HATEOAS.
	// rel: “self” (representa uma auto referência do objeto em questão)
	// href: URL (Define o identificador único do recurso)
	String returnType = "LINK";

	public String assinaXMLDiplomado(HttpServletRequest request) throws IOException {

		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");

		Resource resourceOriginalDocument = null;

		//Obtem o documento PDF
		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}
		InputStream inputStream = resourceOriginalDocument.getInputStream();
		
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_XMLDiplomado.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_XMLDiplomado.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		HttpHeaders headers = new HttpHeaders();

		String tipoAssinante = "";
		if(request.getParameterValues("tipoAssinatura") != null)
			tipoAssinante = request.getParameterValues("tipoAssinatura")[0];
		
		// Configurando as credenciais do KMS BRy (Ou DINAMO/GOVBR)
		String kmsType = "";
		if(request.getParameterValues("kms_type") != null) {
			kmsType = request.getParameterValues("kms_type")[0];
			headers.set("kms_type", kmsType); // Available values : BRYKMS, GOVBR, DINAMO
		}

		JSONObject kmsData = new JSONObject();

		String kmsTypeSistema = "";
		if(request.getParameterValues("kms_type_credential") != null)
			kmsTypeSistema = request.getParameterValues("kms_type_credential")[0];

		// Valor da Credencial
		switch (kmsTypeSistema) {
		case "PIN":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;
		case "TOKEN":
			kmsData.put("token", request.getParameterValues("valor_credencial")[0]);
			break;
		case "OTP":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;
		}

		// UUID do Certificado no KMS BRy / DINAMO
		if (request.getParameterValues("uuid_cert") != null)
			kmsData.put("uuid_cert", request.getParameterValues("uuid_cert")[0]);
		// CPF do usuario do KMS ou nome de usuário (login) do DINAMO
		if (request.getParameterValues("user") != null)
			kmsData.put("user", request.getParameterValues("user")[0]);
		// UUID da chave privada do certificado caso seja DINAMO
		if (kmsType.equals("DINAMO"))
			if (request.getParameterValues("uuid_cert") != null)
				kmsData.put("uuid_pkey", request.getParameterValues("uuid_pkey")[0]);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		
		headers.add("Authorization", authorization);
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
		
		form.add("kms_data", kmsData.toString());

		// NONCE DA REQUISIÇÃO. NONCE É UM NÚMERO QUALQUER PARA IDENTIFICAÇÃO E CONTROLE
		// DA REQUISIÇÃO.
		form.add("nonce", 1);

		// FORMATO DE ASSINATURA * DEIXAR SEMPRE 'ENVELOPED' *
		form.add("signatureFormat", "ENVELOPED");

		// ALGORITMO HASH * DEIXAR SEMPRE 'SHA256' *
		form.add("hashAlgorithm", "SHA256");

		form.add("returnType", returnType);

		// Abaixo estão os parâmetros que devem ser configurados n vezes em caso de
		// assinatura em lote.
		// Cada posição do array pode conter uma configuração diferente.
		// Se não for lote, basta configurar n = 0.

		// NONCE DO DOCUMENTO NA REQUISIÇÃO. NONCE É UM NÚMERO QUALQUER PARA
		// IDENTIFICAÇÃO E CONTROLE DA REQUISIÇÃO.
		form.add("originalDocuments[0][nonce]", 1);

		// DIPLOMA QUE DEVE SER ASSINADO, DEVE SER ADICIONADO O CAMINHO ATÉ O DIPLOMA,
		// NESTE EXEMPLO O CAMINHO É PEGO ATRAVÉS DO FRONT-END DO EXEMPLO
		form.add("originalDocuments[0][content]", new FileSystemResource(".temp/temp_XMLDiplomado.xml"));

		// Tipo de assinante enviado pelo front-end (será usado no switch-case para
		// diferentes tipos de assinantes do diploma)

		switch (tipoAssinante) {

		case "Representantes":

			System.out.println("Tipo de Assinatura: Decano ou Reitor");

			// PERFIL DE ASSINATURA * DEIXAR SEMPRE 'ADRC' PARA ASSINATURAS DE
			// REPRESENTANTES DE 'IES' *
			form.add("profile", "ADRC");

			// Abaixo estão os parâmetros que devem ser configurados n vezes em caso de
			// assinatura em lote.
			// Cada posição do array pode conter uma configuração diferente.
			// Se não for lote, basta configurar n = 0.

			// DEIXAR COMO FIXO 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");

			// DEIXAR FIXO COMO 'DadosRegistro' PARA ASSINATURAS DE REPRESENTANTES DA IES *
			// PODE SER NECESSÁRIO ALTERAR ESTE PARÂMETRO PARA DadosDiplomaNSF DE ACORDO COM O ARQUIVO SENDO UTILIZADO.
			form.add("originalDocuments[0][specificNode][name]", "DadosRegistro");

			break;

		case "IESRegistradora":

			System.out.println("Tipo de Assinatura: IESRegistradora");

			// PERFIL DE ASSINATURA * DEIXAR SEMPRE 'ADRA' PARA ASSINATURA DA ENTIDADE QUE
			// REGISTRA OS DIPLOMAS
			form.add("profile", "ADRA");

			form.add("includeXPathEnveloped", "false");

			break;
		}

		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);

		try {
			System.out.println("Iniciado processo de assinatura...");

			if (returnType == "LINK") {
				ResponseEntity<String> response = restTemplate.exchange(
						url, HttpMethod.POST, entity,
						String.class);
				System.out.println("Assinatura realizada com sucesso!");
				System.out.println("Retornando link para download");
				
				tempFile.delete();
				
				return response.getBody();
			} else {

				ResponseEntity<String[]> response = restTemplate.exchange(
						url, HttpMethod.POST, entity,
						String[].class);
				System.out.println("Assinatura realizada com sucesso!");

				byte[] assinatura = Base64.getDecoder().decode(response.getBody()[0]);
				File diploma = new File(
						"XMLsAssinados/" + "exemplo-diploma-diplomado-assinado.xml");
				FileOutputStream diplomaAssinado = new FileOutputStream(diploma);
				diplomaAssinado.write(assinatura);
				diplomaAssinado.close();
				System.out.println("Arquivo salvo localmente em: " + diploma.getAbsolutePath());

				tempFile.delete();
				
				return "{\"message\": \"Diploma assinado por " + tipoAssinante + " e armazenado localmente em: " + diploma.getAbsolutePath()
						+ "\"}";
			}
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			String mensagem = "Mensagem de erro recebida: " + e.getResponseBodyAsString();
			JSONObject json = new JSONObject(e.getResponseBodyAsString());
			
			throw new ResponseStatusException(e.getStatusCode(), mensagem);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Não foi possível decodificar mensagem de erro recebida da API.");
		}

	}
	
	public String assinaXMLDocumentacao(HttpServletRequest request) throws IOException {

		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");

		Resource resourceOriginalDocument = null;

		//Obtem o documento PDF
		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}
		InputStream inputStream = resourceOriginalDocument.getInputStream();
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_XMLDocumentacaoAcademica.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_XMLDocumentacaoAcademica.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		
		headers.add("Authorization", authorization);
		// Configurando as credenciais do KMS BRy (Ou DINAMO/GOVBR)
		String kmsType = "";
		if (request.getParameterValues("kms_type") != null) {
			kmsType = request.getParameterValues("kms_type")[0];
			System.out.println(kmsType);
			headers.add("kms_type", kmsType); // Available values : BRYKMS, GOVBR, DINAMO
		}

		JSONObject kmsData = new JSONObject();

		String kmsTypeSistema = "";
		if (request.getParameterValues("uuid_cert") != null)
			kmsTypeSistema = request.getParameterValues("uuid_cert")[0];

		String tipoAssinante = "";
		if(request.getParameterValues("tipoAssinatura") != null)
			tipoAssinante = request.getParameterValues("tipoAssinatura")[0];
		
		// Valor da Credencial
		switch (kmsTypeSistema) {
		case "PIN":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;
		case "TOKEN":
			kmsData.put("token", request.getParameterValues("valor_credencial")[0]);
			break;
		case "OTP":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;
		}

		// UUID do Certificado no KMS BRy / DINAMO
		if (request.getParameterValues("uuid_cert") != null)
			kmsData.put("uuid_cert", request.getParameterValues("uuid_cert")[0]);
		// CPF do usuario do KMS ou nome de usuário (login) do DINAMO
		if (request.getParameterValues("user") != null)
			kmsData.put("user", request.getParameterValues("user")[0]);
		// UUID da chave privada do certificado caso seja DINAMO
		if (kmsType.equals("DINAMO"))
			if (request.getParameterValues("uuid_cert") != null)
				kmsData.put("uuid_pkey", request.getParameterValues("uuid_pkey")[0]);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		headers.add("Authorization", authorization);
		headers.add("kms_type", kmsType);
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

		form.add("kms_data", kmsData.toString());

		// FORMATO DE ASSINATURA * DEIXAR SEMPRE 'ENVELOPED' *
		form.add("signatureFormat", "ENVELOPED");

		// ALGORITMO HASH * DEIXAR SEMPRE 'SHA256' *
		form.add("hashAlgorithm", "SHA256");

		form.add("returnType", returnType);

		// Abaixo estão os parâmetros que devem ser configurados n vezes em caso de
		// assinatura em lote.
		// Cada posição do array pode conter uma configuração diferente.
		// Se não for lote, basta configurar n = 0.

		// NONCE DO DOCUMENTO NA REQUISIÇÃO. NONCE É UM NÚMERO QUALQUER PARA
		// IDENTIFICAÇÃO E CONTROLE DA REQUISIÇÃO.
		form.add("originalDocuments[0][nonce]", 1);

		// DIPLOMA QUE DEVE SER ASSINADO, DEVE SER ADICIONADO O CAMINHO ATÉ O DIPLOMA,
		// NESTE EXEMPLO O CAMINHO É PEGO ATRAVÉS DO FRONT-END DO EXEMPLO
		form.add("originalDocuments[0][content]", new FileSystemResource(".temp/temp_XMLDocumentacaoAcademica.xml"));

		// Tipo de assinante enviado pelo front-end (será usado no switch-case para
		// diferentes tipos de assinantes do diploma)

		switch (tipoAssinante) {

		case "Representantes":

			System.out.println("Tipo de Assinatura: IESEmissora - Privado");

			// PERFIL DE ASSINATURA * DEIXAR SEMPRE 'ADRC' PARA ASSINATURAS DE
			// REPRESENTANTES DE 'IES' *
			form.add("profile", "ADRC");

			// Abaixo estão os parâmetros que devem ser configurados n vezes em caso de
			// assinatura em lote.
			// Cada posição do array pode conter uma configuração diferente.
			// Se não for lote, basta configurar n = 0.

			// DEIXAR COMO FIXO 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");

			// DEIXAR FIXO COMO 'DadosDiploma' PARA ASSINATURAS DE REPRESENTANTES DA IES *
			// PODE SER NECESSÁRIO ALTERAR ESTE PARÂMETRO PARA DadosDiplomaNSF DE ACORDO COM O ARQUIVO SENDO UTILIZADO.
			form.add("originalDocuments[0][specificNode][name]", "DadosDiploma");

			break;

		case "IESEmissoraDadosDiploma":

			System.out.println("Tipo de Assinatura: IES Registradora - Privado");

			// DEIXAR COMO FIXO 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");

			// DEIXAR FIXO COMO 'DadosDiploma' PARA ASSINATURAS DE REPRESENTANTES DA IES *
			form.add("originalDocuments[0][specificNode][name]", "DadosDiploma");
			
			// PERFIL DE ASSINATURA * DEIXAR SEMPRE 'ADRC' PARA ASSINATURAS DE
			// REPRESENTANTES DA 'IESEmissora' *
			form.add("profile", "ADRC");
			form.add("includeXPathEnveloped", "false");

			break;
		
		case "IESEmissoraRegistro":

			System.out.println("Tipo de Assinatura: IES Registradora - Privado");

			// PERFIL DE ASSINATURA * DEIXAR SEMPRE 'ADRC' PARA ASSINATURAS DE
			// REPRESENTANTES DA 'IESEmissora' *
			form.add("profile", "ADRA");
			form.add("includeXPathEnveloped", "false");

			break;
		}

		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);

		try {
			System.out.println("Iniciado processo de assinatura...");

			if (returnType == "LINK") {
				ResponseEntity<String> response = restTemplate.exchange(
						url, HttpMethod.POST, entity,
						String.class);
				System.out.println("Assinatura realizada com sucesso!");
				System.out.println("Retornando link para download");
				
				tempFile.delete();
				
				return response.getBody();
			} else {

				ResponseEntity<String[]> response = restTemplate.exchange(
						url, HttpMethod.POST, entity,
						String[].class);
				System.out.println("Assinatura realizada com sucesso!");

				byte[] assinatura = Base64.getDecoder().decode(response.getBody()[0]);
				File diploma = new File(
						"XMLsAssinados/" + "exemplo-diploma-documentacao-assinado.xml");
				FileOutputStream diplomaAssinado = new FileOutputStream(diploma);
				diplomaAssinado.write(assinatura);
				diplomaAssinado.close();
				System.out.println("Arquivo salvo localmente em: " + diploma.getAbsolutePath());

				tempFile.delete();
				
				return "{\"message\": \"Diploma assinado por " + tipoAssinante + " e armazenado localmente em: " + diploma.getAbsolutePath()
						+ "\"}";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public String copiaNodo(MultipartFile documentacaoAcademica, MultipartFile xMLDiplomado)
			throws IOException, ParserConfigurationException, SAXException,
			TransformerFactoryConfigurationError, TransformerException {
		
		InputStream inputStream = documentacaoAcademica.getInputStream();
		File tempFile = new File(".temp/temp_documentacaoAcademica.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_documentacaoAcademica.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		
		InputStream inputStream2 = xMLDiplomado.getInputStream();
		File tempFile2 = new File(".temp/temp_XMLDiplomado.xml");
		if(tempFile2.exists()) {
			tempFile2.delete();
			if(!tempFile2.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario no diretorio .temp/temp_XMLDiplomado.xml");
		}
		FileOutputStream outputStream2 = new FileOutputStream(tempFile2);
		buffer = new byte[2048];
		while ((bytesRead = inputStream2.read(buffer)) != -1) {
			outputStream2.write(buffer, 0, bytesRead);
		}
		outputStream2.close();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		
		DocumentBuilder db = dbf.newDocumentBuilder();
		
		Document doc = db.parse(new FileInputStream(new File(".temp/temp_documentacaoAcademica.xml")));
		doc.getDocumentElement().normalize();
		
		Node registroReqNode = doc.getElementsByTagName("RegistroReq").item(0);
		Element registroReq = (Element) registroReqNode;
		
		NodeList dadosDiplomaList = registroReq.getElementsByTagName("DadosDiploma");
		if (dadosDiplomaList.getLength() == 0) {
			dadosDiplomaList = registroReq.getElementsByTagName("DadosDiplomaNSF");
		}
		Node dadosDiplomaNode = dadosDiplomaList.item(0);
						
		DocumentBuilder dbDiplomado = dbf.newDocumentBuilder();
		Document docDiplomado = dbDiplomado.parse(new FileInputStream(new File(".temp/temp_XMLDiplomado.xml")));
		
		docDiplomado.getDocumentElement().normalize();
		
		Node infDiplomaNode = docDiplomado.getElementsByTagName("infDiploma").item(0);
		Element infDiploma = (Element) infDiplomaNode;
		
		infDiploma.insertBefore(docDiplomado.adoptNode(dadosDiplomaNode.cloneNode(true)), infDiploma.getFirstChild());
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		Result output = new StreamResult(new File("XMLsAssinados/XMLDiplomadoCompleto.xml"));
		Source input = new DOMSource(docDiplomado);
		System.out.println(docDiplomado.toString());	
		transformer.transform(input, output);
		
		File file = new File("XMLsAssinados/XMLDiplomadoCompleto.xml");
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		buffer = new byte[2048];
		while ((bytesRead = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, bytesRead);
		}
		fis.close();
		
		tempFile.delete();
		tempFile2.delete();
		
		byte[] xMLDiplomadoCompleto = baos.toByteArray();
		
		return new String(xMLDiplomadoCompleto, "UTF-8");
	}
}
